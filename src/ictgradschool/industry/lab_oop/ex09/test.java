package ictgradschool.industry.lab_oop.ex09;

public class test {
    public void start() {
        Deodorant myDeodorant = new Deodorant("Gentle", "Baby Powder", true,
                4.99);
        Deodorant yourDeodorant = new Deodorant("Spring", "Blossom", false,
                3.99);

        System.out.println("1. " + myDeodorant.toString());
        myDeodorant.setBrand("Sweet");
        yourDeodorant.setPrice(5.29);
        System.out.println("2. " + yourDeodorant.toString());
        if (myDeodorant.isRollOn()) {
            System.out.println("3. Roll On");
        } else {
            System.out.println("3. Spray");
        }
        System.out.println("4. " + myDeodorant.toString());
        if (yourDeodorant.isMoreExpensiveThan(myDeodorant)) {
            System.out.println("5. Most expensive is " +
                    yourDeodorant.getBrand());
        } else {
            System.out.println("5. Most expensive is " +
                    myDeodorant.getBrand());
        }
    }

    public static void main(String[] args) {

        test ex = new test();
        ex.start();

    }
}
