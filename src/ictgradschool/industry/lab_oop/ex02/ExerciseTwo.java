package ictgradschool.industry.lab_oop.ex02;

import ictgradschool.Keyboard;

/**
 * Write a program that prompts the user to enter a range – 2 integers representing a lower bound and an upper bound.
 * You should use Keyboard.readInput() for this. Then, convert these bounds from String to int using Integer.parseInt().
 * Your program should then use Math.random() to generate 3 random integers that lie between the range entered (inclusive),
 * and then use Math.min() to determine which of the random integers is the smallest.
 */
public class ExerciseTwo {

    /**
     * TODO Your code here. You may also write additional methods if you like.
     */


    private void start() {
        int low = readUser("Enter low");
        int high = readUser("enter high");

        int a = (int)(Math.random() * ((high + 1 - low))) + low;
        int b = (int)(Math.random() * ((high + 1 - low))) + low;
        int c = (int)(Math.random() * ((high + 1 - low))) + low;


        int lowest = getLowestInt(a,b,c);

        System.out.println("3 randomly generated numbers: " + a + " " + b + " " + c);
        System.out.println("Smallest number is " + lowest);

    }

    public int getLowestInt(int a, int b, int c){
        int x = Math.min(a,b);
        int y = Math.min(a,c);
        int z = Math.min(x,y);
        return z;

    }

    public int readUser(String prompt){
        System.out.println(prompt);
        String userInput = Keyboard.readInput();
        int userInt = Integer.parseInt(userInput);
        return userInt;
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        ExerciseTwo ex = new ExerciseTwo();
        ex.start();

    }
}
