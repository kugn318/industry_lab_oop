package ictgradschool.industry.lab_oop.ex07;
import ictgradschool.Keyboard;

public class Ex_08{

    public static void main(String[] args) {
        Ex_08 ex = new Ex_08();
        ex.start();
    }

    public void start() {

        double radius;
        System.out.println("\"Volume of a Sphere\"");
        System.out.println("Enter the radius: ");

        radius = Double.parseDouble(Keyboard.readInput());

        double volume = ((4 * Math.PI) * Math.pow(radius,3))/3;

        System.out.println("Volume: " + volume);
        }
}