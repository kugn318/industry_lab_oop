package ictgradschool.industry.lab_oop.ex07;

import ictgradschool.industry.lab_oop.ex02.ExerciseTwo;

public class Ex_07 {


    public void start() {
        String colours, first, second, third;
        int position1, position2, position3, length;
        colours = "redorangeyellow";
        first = colours.substring(4, 9);
        second = colours.substring(0, 4);
        third = colours.charAt(0) + colours.substring(13);
        length = third.length();
        third = third.toUpperCase();
        position1 = colours.indexOf('A');
        position2 = colours.indexOf("el");
        position3 = colours.indexOf("or");
        System.out.println("first: " + first);
        System.out.println("second: " + second);
        System.out.println("third: " + third);
        System.out.println("length: " + length);
        System.out.println("position1: " + position1);
        System.out.println("position2: " + position2);
        System.out.println("position3: " + position3);
    }

    public static void main(String[] args) {

        Ex_07 ex = new Ex_07();
        ex.start();

    }

}
